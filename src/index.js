import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'

class Score extends React.Component{

  constructor(props){
    super(props);
    this.state = {RMA:null,BAR:null,MP:null,TRM:null,TBAR:null, };

  }


  handleclick(){
    var rma= Math.floor(Math.random() * 10);
    var bar=Math.floor(Math.random() * 10);
    this.setState({RMA:rma,BAR:bar,MP:(this.state.MP+1),TRM:(this.state.TRM+rma),TBAR:(this.state.TBAR+bar)});

  }

  render(){
    return(
      <div className='score'>
       <p>SCORE</p>
       <p>RMA:{this.state.RMA}-{this.state.BAR}:BAR</p>
       <button className='start match' onClick = {()=>this.handleclick()}>
        START ELCLASICO
       </button>
       <Player mp ={this.state.MP} goals={this.state.TRM} name ={'Ronaldo'} />
       <Player mp ={this.state.MP} goals={this.state.TBAR} name ={'Messi'} />
      </div>
    );
  }
}



class Player extends React.Component {
  render(){
    return(
      <div className='player'>
       <p>{this.props.name}</p>
       <p>Matches Played:{this.props.mp}</p>
       <p>Goals:{this.props.goals}</p>
      </div>
    );
  }
}

class App extends React.Component {
  render() {
    return (
      <div>
        <h1>Home</h1>
      </div>
    )
  }
}

const routing = (
  <Router>
   <div className='router'>
    <Route exact path='/' component={App}/>
    <Route path='/elclasico' component={Score}/>
   </div>
  </Router>
)

ReactDOM.render(routing , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
